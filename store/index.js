// import Vue from 'vue'
import Vuex from 'vuex'

import Category from './Category/index.js'
import Product from './Product/index'
// Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    Category,
    Product
  },
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  }
})
