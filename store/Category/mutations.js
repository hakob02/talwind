export default {
  SET_CATEGORIES (state, payload) {
    state.categories = payload.categories.data
  },
  SET_CATEGORY (state, payload) {
    state.category = payload.data.category
  }
}
