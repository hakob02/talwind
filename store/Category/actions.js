export default {
  async categories ({ commit }, data) {
    await commit('SET_CATEGORIES', data)
  },
  async category ({ commit }, data) {
    await commit('SET_CATEGORY', data)
  }
}
