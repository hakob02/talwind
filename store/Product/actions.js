export default {
  async products ({ commit }, data) {
    await commit('SET_PRODUCTS', data)
  },
  async product ({ commit }, data) {
    await commit('SET_PRODUCT', data)
  }
}
