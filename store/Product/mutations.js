export default {
  SET_PRODUCTS (state, payload) {
    state.products = payload.data.products.data
  },
  SET_PRODUCT (state, payload) {
    state.product = payload.data.product
  }
}
