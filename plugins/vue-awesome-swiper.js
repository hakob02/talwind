import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'

// import custom style
// import 'swiper/css'
import 'swiper/swiper-bundle.min.css'
Vue.use(VueAwesomeSwiper)
