export default (context) => {
  return {
    httpEndpoint: 'http://127.0.0.1:8000/graphql',

    /*
     * For permanent authentication provide `getAuth` function.
     * The string returned will be used in all requests as authorization header
     */
    getAuth: () => 'Bearer my-static-token'
  }
}
